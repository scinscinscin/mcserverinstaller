#!/bin/bash

#MCBedrockInstaller Written by Scinorandex
#Do whatever you want with the code just credit me
serverdir=$PWD/mcbedrockserver

#Adds color to the outputs
head='\033[0;35m'
body='\033[0;33m'
NC='\033[0m'
error='\033[0;31m'

function ctrl_c() {
    echo 
    echo -e "${error}Ctrl-C trapped. Deleting ${head}${serverdir}${error} and exiting.${NC}"
    rm -rf "${serverdir}"
    killscript
}

function killscript(){
    echo -e "${body}Script has finished${NC}"
    exit
}

function checkIfSure(){
    echo -e "${body}Are you sure you want to install a $1 server ${head}[Yy/Nn]${body}?${NC}"
    read -n 1 -r
    echo "" 
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then
        rm -rf "${serverdir}"
    killscript
    fi    
    echo -e "${body}Will now install a $1 server${NC}"
}

		echo -e "${body}By running this script you are indicating your agreement to Mojang’s EULA at:${NC}"
		echo -e "https://account.mojang.com/documents/minecraft_eula"
		echo -e "${body}Press Ctrl+C to quit or press any key to continue${NC}"
		read -n 1 -r
		echo ""
		echo -e "${body}Would you like to install dependencies? The script WILL fail if dependencies aren't installed. ${head}[Yy/Nn]${NC}"
		read -n 1 -r
    		echo "" 
		if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    		echo -e "${body}OK, Script will proceed.${NC}"
		else
    		    echo -e "${body}What kind of distro are you using?${NC}"
    		    select dist in "arch-based" "ubuntu-based"; do
     		        case $dist in
                            arch-based ) sudo pacman --noconfirm unzip jre8-openjdk-headless; break;;
        		    ubuntu-based ) sudo apt-get -y install unzip openjdk-8-jre-headless; break;;
    		    	esac
    		    done
		fi

if [ -d "${serverdir}" ]; then
    echo -e "${head}${serverdir}${error} exists.${NC}"
    echo -e "${body}Would you like the script to remove ${head}${serverdir}${body}?${NC}"
    select choice in "yes" "no" ;do
    case $choice in
        yes ) echo -e "${body}Warning: Are you sure you want to delete ${head}${serverdir}${body}?${NC}"
        echo -e "${head}${serverdir}${body} will be lost forever (A long time!)${NC}"
        select choice2 in "no" "yes" ; do
            case $choice2 in
                yes ) rm -rf "${serverdir}" 
                 break
                 ;;
            no ) killscript
            esac
        done
        break
        ;;
        no ) killscript
    esac
    done
fi 

#Trap control+c
trap ctrl_c INT
mkdir -p "${serverdir}"
cd "${serverdir}" || return

echo -e "${body}Do you want to install ${head}PocketMine, Nukkit, or Bedrock Dedicated Server?${NC}"
select server in "pocketmine" "nukkit" "dedicated"; do
    case $server in
	pocketmine ) echo -e "${body}You have chosen ${head}pocketmine${NC}"
		checkIfSure "PocketMine"
		wget -O binary.tar.gz https://jenkins.pmmp.io/job/PHP-7.3-Aggregate/lastSuccessfulBuild/artifact/PHP-7.3-Linux-x86_64.tar.gz
		tar xzvf binary.tar.gz 
		rm binary.tar.gz 
		wget https://jenkins.pmmp.io/job/PocketMine-MP/lastSuccessfulBuild/artifact/PocketMine-MP.phar
		wget https://raw.githubusercontent.com/pmmp/PocketMine-MP/master/start.sh
		break;;
	nukkit ) echo -e "You have chosen ${head}nukkit${NC}"
		wget -O nukkit.jar https://ci.nukkitx.com/job/NukkitX/job/Nukkit/job/master/lastSuccessfulBuild/artifact/target/nukkit-1.0-SNAPSHOT.jar
		echo "java -jar nukkit.jar" >> start.sh
		break;;
	dedicated ) echo -e "${body}You have chosen ${head}bedrock dedicated server${NC}"
		wget -O bedrock.zip https://minecraft.azureedge.net/bin-linux/bedrock-server-1.16.0.2.zip
		unzip bedrock.zip
		rm bedrock.zip
		echo "LD_LIBRARY_PATH=. ./bedrock_server" >> start.sh
		break;;
    esac
done
chmod +x start.sh

#stop trapping control+c
trap - 1 2 3 15
trap

echo -e "${body}Server is done installing, ${head}Choose to start the server, or exit${NC}"
select post in "start" "exit"; do
    case $post in
        start ) echo -e "${body}Starting server now${NC}"
                ./start.sh
                break;;
        exit ) break;;
    esac
done

echo -e "Script has finished."
