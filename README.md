# mcserverinstaller
A lightweight minecraft server jar downloader / installer for both Java and Bedrock written in Bash

# Functions

## Java
Downloads the latest server / buildtools for **Vanilla, Fabric, CraftBukkit, Spigot, Paper, Tuinity, Forge, SpongeVanilla, Mohist, and Magma**.

Runs the Build tools of **Forge, Fabric, CraftBukkit, and Spigot**.

Creates a start script to start the server with [Akair's flags](https://aikar.co/2018/07/02/tuning-the-jvm-g1gc-garbage-collector-flags-for-minecraft/)

It also allows you to create a systemd service file to autostart at boot time

### Supported Versions
|Server    |       Vanilla       |     Fabric      |CraftBukkit|  Spigot |Paper|Tuinity| 
|:--------:|:-------------------:|:---------------:|:---------:|:-------:|:---:|:-----:|
|Support   |         1.3+        |      1.14+      |    1.8+   |   1.8+  |1.12+| 1.15.2|
|Notes     |prereleases/snapshots|     18w43b+     |           |         |     |       |
|          |                     |                 |           |         |     |       |
|**Server**|      **Forge**      |**SpongeVanilla**|**Mohist** |**Magma**|
|Support   |        1.15.2       |     1.12.2      |  1.12.2   |  1.12.2 |
|Notes     |     1.15.2-31.2.5   |1.12.2-2838-7.2.2|           |         |

## Bedrock

Downloads the latest server software for **Bedrock's Dedicated Server, PocketMine, and Nukkit**.

Creates a start script to run the server depending on the server software.

### Supported Versions
|Server    |Dedicated Server|PocketMine|Nukkit| 
|:--------:|:--------------:|:--------:|:----:|
|Support   |    1.16.0.2    |  Latest  |Latest|

# Why
Why not?

It's an easy script that allows you to install most if not all the server jar's you'd want to for both editions of Minecraft
