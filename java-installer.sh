#!/bin/bash

#MCJavaInstaller Written by Scinorandex
#Do whatever you want with the code just credit me

serverdir=$PWD/mcserver
serverjar="server"
maxram=$(($(free -m | grep Mem: | awk '{print $2}') - 1000))

#Adds color to the outputs
head='\033[0;35m'
body='\033[0;33m'
NC='\033[0m'
error='\033[0;31m'

function ctrl_c() {
    echo 
    echo -e "${error}Ctrl-C trapped. Deleting ${head}${serverdir}${error} and exiting.${NC}"
    rm -rf "${serverdir}"
    killscript
}

function killscript(){
    echo -e "${body}Script has finished${NC}"
    exit
}

function checkIfSure(){
    echo -e "${body}Are you sure you want to install a $1 ${head}$2${body} server ${head}[Yy/Nn]${body}?${NC}"
    read -n 1 -r
    echo "" 
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then
        rm -rf "${serverdir}"
	killscript
    fi	
    echo -e "${body}Will now install a $1 ${head}$2${body} server${NC}"
}

function getLatestBuild(){
    wget "$1"
    unzip archive.zip
    mv archive/build/distributions/* .
    rm -rf archive*
}

function bukkitSpigot(){
    echo -e "${body}Select which version you want to install, Only works for versions supported by BuildTools${NC}"
    echo -e "${body}Ex: ${head}1.8, 1.15.2, default is latest${NC}"
	while :
	do
 	    read -r version
   	    if [[ ${version} == "" ]]; then version="latest"; fi
            if curl -s -o /dev/null -w "%{http_code}" curl https://hub.spigotmc.org/versions/${version}.json | grep 200 >> /dev/null ; then
        	checkIfSure "$1" "${version}"
    		break
	    else 	
		echo "${head}${version}${body} is not a $1 version, Please try again.${NC}"
	    fi
        done
}
echo -e "${body}By running this script you are indicating your agreement to Mojang’s EULA at:${NC}"
echo -e "https://account.mojang.com/documents/minecraft_eula"
echo -e "${body}Press Ctrl+C to quit or press any key to continue${NC}"
read -n 1 -r
echo ""

echo -e "${body}Would you like to install dependencies? The script WILL fail if dependencies aren't installed. ${head}[Yy/Nn]${NC}"
read -n 1 -r
    echo "" 
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    echo -e "${body}OK, Script will proceed.${NC}"
else
    echo -e "${body}What kind of distro are you using?${NC}"
    select dist in "arch-based" "ubuntu-based"; do
        case $dist in
	    arch-based ) sudo pacman --noconfirm -S jre8-openjdk-headless git jq unzip curl; break;;
	    ubuntu-based ) sudo apt-get -y install openjdk-8-jre-headless git jq unzip curl; break;;
	esac
    done
fi	

if [ -d "${serverdir}" ]; then
    echo -e "${head}${serverdir}${error} exists.${NC}"
    echo -e "${body}Would you like the script to remove ${head}${serverdir}${body}?${NC}"
    select choice in "yes" "no" ;do
	case $choice in
		yes ) echo -e "${body}Warning: Are you sure you want to delete ${head}${serverdir}${body}?${NC}"
		echo -e "${head}${serverdir}${body} will be lost forever (A long time!)${NC}"
		select choice2 in "no" "yes" ; do
		    case $choice2 in
		        yes ) rm -rf "${serverdir}" 
			     break
			     ;;
			no ) killscript
		    esac
		done
		break
		;;
	    no ) killscript
	esac
    done
fi 

#Trap control+c
trap ctrl_c INT
mkdir -p "${serverdir}"
cd "${serverdir}" || return

echo -e "${body}Do you want to install ${head}vanilla, forge, fabric, spongevanilla, craftbukkit, spigot, paper, tuinity, mohist, or magma?${NC}"
select server in "vanilla" "forge" "fabric" "spongevanilla" "craftbukkit" "spigot" "paper" "tuinity" "mohist" "magma"; do
    case $server in
	spongevanilla ) echo -e "${body}You have chosen ${head}spongevanilla${NC}"
		checkIfSure "SpongeVanilla" "1.12.2"
		wget https://repo.spongepowered.org/maven/org/spongepowered/spongevanilla/1.12.2-7.2.2/spongevanilla-1.12.2-7.2.2.jar
		break;;
	tuinity ) echo -e "${body}You have chosen ${head}tuinity${NC}"
		checkIfSure "Tuinity" "1.15.2"
		wget https://ci.codemc.io/job/Spottedleaf/job/Tuinity/lastSuccessfulBuild/artifact/tuinity-paperclip.jar
		break;;
        craftbukkit ) echo -e "${body}You have chosen ${head}craftbukkit${NC}"
		bukkitSpigot "CraftBukkit"
		wget https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
		java -jar BuildTools.jar --compile craftbukkit --rev ${version}
		rm BuildTools.jar
		break;;
        spigot ) echo -e "${body}You have chosen ${head}spigot${NC}"
		bukkitSpigot "Spigot"
		wget https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
		java -jar BuildTools.jar --rev ${version} 
		rm BuildTools.jar 
		rm craftbukkit-${version}.jar 2> /dev/null
	        break;;
        paper ) echo -e "${body}You have chosen ${head}paper${NC}"
		echo -e "${body}What version of Paper would you like to install?${NC}"
		echo -e "${body}Only use major versions. Ex:${head} 1.12, 1.13, 1.14, 1.15${NC}"
                while :
                do
                    read -r version
                    if [[ ${version} == "1.12" ]]; then
			checkIfSure "Paper" "1.12"
                        wget https://papermc.io/ci/job/Paper/lastSuccessfulBuild/artifact/paperclip.jar
			break
                    elif curl -s -o /dev/null -w "%{http_code}" curl "https://papermc.io/ci/job/Paper-${version}/lastSuccessfulBuild/artifact/paperclip.jar" | sed '/Jetty/d' | grep 200 >> /dev/null ; then
                        checkIfSure "Paper" "${version}"
			wget "https://papermc.io/ci/job/Paper-${version}/lastSuccessfulBuild/artifact/paperclip.jar"
                        break
                    else     
		        echo -e "${head}${version}${body} is not a Paper version, Please try again.${NC}"
       		    fi
		done 
		break;;
        vanilla ) echo -e "${body}You have chosen ${head}vanilla${NC}"
	    	echo -e "${body}Select which version you want to install, Only works with release 1.3+. Also works with snapshots and pre-releases.${NC}"
		echo -e "${body}Ex:${head} 1.8.9, 1.15.2, 13w16a, 1.9-pre1${NC}"
		while :
		do 
		    read -r version
		    json0=$(curl -s https://launchermeta.mojang.com/mc/game/version_manifest.json | jq  -r --arg version "$version" '.versions[] | select(.id==$version)' | grep url | awk '{ print $2 }')
		    if [[ "${json0}" == "" ]] || [[ "${json0}" == "null" ]]; then 
    		        echo -e "${error}This version is invalid, Please try again.${NC}"
		    else
 		        json0=${json0//,}
    		        json0="${json0%\"}"
    		        json0="${json0#\"}"
    		        dllink=$(curl -s "${json0}" | jq .downloads.server.url)
    		        if [[ "${dllink}" == "null" ]]; then
        	            echo -e "${error}This version is before release 1.3, Please try again.${NC}"
    		        else	
    		            dllink="${dllink%\"}"
    		            dllink="${dllink#\"}"
			    checkIfSure "Vanilla" "${version}"
			    break
    		        fi
	   	    fi
		done
  		echo "Will be installing ${version} from ${dllink}"
     	        wget "${dllink}"
		break;;
	forge ) echo -e "${body}You have chosen ${head}forge${NC}"
		checkIfSure "Forge" "1.15.2"
		wget https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.15.2-31.2.5/forge-1.15.2-31.2.5-installer.jar
		java -jar ./forge-*.jar --installServer
		rm forge-*.jar
		break;;
        mohist ) echo -e "${body}You have chosen ${head}mohist${NC}"
		checkIfSure "Mohist" "1.12.2"
		getLatestBuild "https://ci.codemc.io/job/Mohist-Community/job/Mohist-1.12.2/lastSuccessfulBuild/artifact/*zip*/archive.zip"
		break;;
	magma ) echo -e "You have chosen ${head}magma${NC}"
		checkIfSure "Magma" "1.12.2"
		getLatestBuild "https://ci.hexeption.dev/job/Magma%20Foundation/job/Magma/job/master/lastSuccessfulBuild/artifact/*zip*/archive.zip"
		break;;
	fabric ) echo -e "${body}You have chosen ${head}fabric${NC}"
		serverjar="fabric"
		echo -e "${body}What version of Fabric do you want to install? From snapshot 18w43b+ or release 1.14+${NC}"
		echo -e "${body}Ex:${head} 20w22a, 1.15${NC}"
		while :
		do
    		read -r version
    		json0=$(curl -s https://launchermeta.mojang.com/mc/game/version_manifest.json | jq  -r --arg version "$version" '.versions[] | select(.id==$version)' | grep releaseTime | awk '{print $2}' )
    		json0="${json0%\"}"
    		json0="${json0#\"}"
    		json0="${json0:0:10}"
   		if [[ "${json0}" == "" ]]; then
    		    echo -e "${error}This is invalid, Please try again.${NC}"    
    		elif [[ "${json0}" <  "2018-10-23" ]] || [ "${version}" == "18w43a" ]; then
        	    echo -e "${error}This version is not supported by Fabric, Please try again${NC}."
    		else
		    checkIfSure "Fabric" "${version}"
        	    break
    		fi
		done
		sleep 5
		wget https://jenkins.modmuss50.me/job/FabricMC/job/fabric-installer/job/master/40/artifact/build/libs/fabric-installer-0.5.2.40.jar
		java -jar ./fabric-installer*.jar server -mcversion "${version}" -downloadMinecraft
		rm ./fabric-installer*.jar
		break;;
    esac
done
if [ "$serverjar" == "server" ]; then
    mv ./*.jar server.jar
else
    mv ./fabric-*.jar fabric.jar
fi
echo -e "${body}Downloaded and moved server.jar${NC}"
while :
do
echo -e "${body}How much RAM should be allocated to the server in MiB? ${head}1024-${maxram}${NC}"
read -r scale
if ! [[ "$scale" =~ ^[0-9]+$ ]]; then
    echo -e "${error}Sorry positive integers only.${NC}"
elif ((scale > maxram ||  scale < 1024)); then
    echo -e "${error}${scale}${body} is outside of range ${head}1024-${maxram}${NC}"
else
    echo "java -Xms${scale}M -Xmx${scale}M -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true -jar ./${serverjar}.jar nogui" >> start.sh
    break
fi
done

chmod +x start.sh
echo "eula=true" >> eula.txt
#stop trapping control+c
trap - 1 2 3 15
trap

echo -e "${body}Server is done installing, ${head}Choose to create/start a systemd service, Start the server, or exit${NC}"
select post in "systemd" "start" "exit"; do
    case $post in
        systemd ) echo -e "${body}You have chosen to ${head}create/enable/start minecraft.service${NC}"
	cd ..
	echo "[Unit]">> sy
	echo "Description=Minecraft Server Systemd Service.">> sy
	echo "">> sy
	echo "[Service]">> sy
	echo "User=$USER">> sy
	echo "Group=$USER">> sy
	echo "Type=simple">> sy
	echo "ExecStart=/bin/bash ${serverdir}/start.sh">> sy
	echo "WorkingDirectory= ${serverdir}">> sy
	echo "[Install]">> sy
	echo "WantedBy=multi-user.target">> sy
	sudo mv sy /etc/systemd/system/minecraft.service
	sudo systemctl enable minecraft.service
	sudo systemctl start minecraft.service
	break;;
	start ) echo -e "${body}Starting server now${NC}"
		./start.sh
		break;;
	exit ) break;;
    esac
done

echo -e "${body}Script has finished.${NC}"
